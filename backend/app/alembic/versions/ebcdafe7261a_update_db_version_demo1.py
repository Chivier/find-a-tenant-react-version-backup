"""update_db_version_demo1

Revision ID: ebcdafe7261a
Revises: e2412789c190
Create Date: 2024-03-24 23:56:39.717101

"""
from alembic import op
import sqlalchemy as sa
import sqlmodel.sql.sqltypes


# revision identifiers, used by Alembic.
revision = 'ebcdafe7261a'
down_revision = 'e2412789c190'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('tenant',
    sa.Column('email', sqlmodel.sql.sqltypes.AutoString(), nullable=False),
    sa.Column('post_code', sqlmodel.sql.sqltypes.AutoString(), nullable=True),
    sa.Column('distance', sa.Float(), nullable=True),
    sa.Column('location', sqlmodel.sql.sqltypes.AutoString(), nullable=True),
    sa.Column('price_lower_bound', sa.Float(), nullable=True),
    sa.Column('price_upper_bound', sa.Float(), nullable=True),
    sa.Column('begin_date', sa.Date(), nullable=True),
    sa.Column('end_date', sa.Date(), nullable=True),
    sa.Column('property_type', sqlmodel.sql.sqltypes.AutoString(), nullable=True),
    sa.Column('room_type', sqlmodel.sql.sqltypes.AutoString(), nullable=True),
    sa.Column('description', sqlmodel.sql.sqltypes.AutoString(), nullable=True),
    sa.Column('let_type', sqlmodel.sql.sqltypes.AutoString(), nullable=True),
    sa.Column('contact_info', sqlmodel.sql.sqltypes.AutoString(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('city', sqlmodel.sql.sqltypes.AutoString(), nullable=False),
    sa.Column('owner_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['owner_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.add_column('item', sa.Column('image_url', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('xhs_link', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('city', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('property_type', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('room_type', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('post_code', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('begin_date', sa.Date(), nullable=True))
    op.add_column('item', sa.Column('end_date', sa.Date(), nullable=True))
    op.add_column('item', sa.Column('duration_description', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('price', sa.Float(), nullable=True))
    op.add_column('item', sa.Column('price_description', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('short_term_allowed', sa.Boolean(), nullable=True))
    op.add_column('item', sa.Column('additional_info', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('contact_info', sqlmodel.sql.sqltypes.AutoString(), nullable=True))
    op.add_column('item', sa.Column('duration', sa.Integer(), nullable=True))
    op.add_column('item', sa.Column('email', sqlmodel.sql.sqltypes.AutoString(), nullable=False))
    op.alter_column('item', 'title',
               existing_type=sa.VARCHAR(),
               nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('item', 'title',
               existing_type=sa.VARCHAR(),
               nullable=False)
    op.drop_column('item', 'email')
    op.drop_column('item', 'duration')
    op.drop_column('item', 'contact_info')
    op.drop_column('item', 'additional_info')
    op.drop_column('item', 'short_term_allowed')
    op.drop_column('item', 'price_description')
    op.drop_column('item', 'price')
    op.drop_column('item', 'duration_description')
    op.drop_column('item', 'end_date')
    op.drop_column('item', 'begin_date')
    op.drop_column('item', 'post_code')
    op.drop_column('item', 'room_type')
    op.drop_column('item', 'property_type')
    op.drop_column('item', 'city')
    op.drop_column('item', 'xhs_link')
    op.drop_column('item', 'image_url')
    op.drop_table('tenant')
    # ### end Alembic commands ###
