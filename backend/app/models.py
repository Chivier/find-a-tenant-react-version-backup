from sqlmodel import Field, Relationship, SQLModel, ARRAY, SQLModel, create_engine, Column, Float
from typing import Optional
from datetime import date


# Shared properties
# TODO replace email str with EmailStr when sqlmodel supports it
class UserBase(SQLModel):
    email: str = Field(unique=True, index=True)
    is_active: bool = True
    is_superuser: bool = False
    full_name: str | None = None


# Properties to receive via API on creation
class UserCreate(UserBase):
    password: str


# TODO replace email str with EmailStr when sqlmodel supports it
class UserCreateOpen(SQLModel):
    email: str
    password: str
    full_name: str | None = None


# Properties to receive via API on update, all are optional
# TODO replace email str with EmailStr when sqlmodel supports it
class UserUpdate(UserBase):
    email: str | None = None  # type: ignore
    password: str | None = None


# TODO replace email str with EmailStr when sqlmodel supports it
class UserUpdateMe(SQLModel):
    full_name: str | None = None
    email: str | None = None


class UpdatePassword(SQLModel):
    current_password: str
    new_password: str


# Database model, database table inferred from class name
class User(UserBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    hashed_password: str
    items: list["Item"] = Relationship(back_populates="owner")
    tenants: list["Tenant"] = Relationship(back_populates="owner")


# Properties to return via API, id is always required
class UserOut(UserBase):
    id: int


class UsersOut(SQLModel):
    data: list[UserOut]
    count: int


# Shared properties
# class ItemBase(SQLModel):
#     title: str
#     post_code: str | None = None
#     description: str | None = None
#     price: float| None = None
#     begin_date: date| None = None
#     end_date: date| None = None
#     duration: int | None = None # Assuming duration is in days
#     property_type: str | None = None
#     room_type: str | None = None
#     image_url: str | None = None  # 新增图片URL字段
#     email: str | None = None

class ItemBase(SQLModel):
    email: str 
    image_url: str | None = None
    xhs_link: str | None = None
    city: str | None = None
    title: str | None = None #房源名称
    property_type: str | None = None
    room_type: str | None = None
    post_code: str | None = None
    begin_date: date | None = None
    end_date: date | None = None
    duration_description: str | None = None
    price: float | None = None
    price_description: str | None = None
    description: str | None = None
    short_term_allowed: bool | None = None
    additional_info: str | None = None
    contact_info: str | None = None
    duration: int | None = None # Assuming duration is in days


class Image(SQLModel, table = True):
    uuid: str | None = Field(default=None, primary_key=True, nullable=False, unique=True, index=True)
    image_url: str | None = None
#     item_id: int | None = Field(default=None, foreign_key="item.id")
#     item: "Item" = Relationship(back_populates="image_url_list")


# Properties to receive on item creation
class ItemCreate(ItemBase):
    email: str
    city: str | None = None

# Properties to receive on item update
class ItemUpdate(ItemBase):
    email: str
#     image_url_list: list["Image"] = Relationship(back_populates="item")


# Database model, database table inferred from class name
class Item(ItemBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    email: str
    owner_id: int | None = Field(default=None, foreign_key="user.id", nullable=False)
    owner: User | None = Relationship(back_populates="items")
#     image_url_list: list["Image"] = Relationship(back_populates="item")


# Properties to return via API, id is always required
class ItemOut(ItemBase):
    id: int
    owner_id: int

    
class ItemsOut(SQLModel):
    data: list[ItemOut]
    count: int
    cities: list[str]


# Shared properties
class TenantBase(SQLModel):
    email: str 
    city: str
    post_code: str | None = None
    distance: float | None = None
    location: str | None = None
    price_lower_bound: float | None = None
    price_upper_bound: float | None = None
    begin_date: date| None = None
    end_date: date| None = None
    property_type: str | None = None
    room_type: str | None = None
    description: str | None = None
    let_type: str | None = None
    contact_info: str | None = None
    

# Properties to receive on item creation
class TenantCreate(TenantBase):
    email: str

# Properties to receive on item update
class TenantUpdate(TenantBase):
    email: str 


# Database model, database table inferred from class name
class Tenant(TenantBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    city: str
    owner_id: int | None = Field(default=None, foreign_key="user.id", nullable=False)
    owner: User | None = Relationship(back_populates="tenants")


# Properties to return via API, id is always required
class TenantOut(TenantBase):
    id: int
    owner_id: int


class TenantsOut(SQLModel):
    data: list[TenantOut]
    count: int
    cities: list[str]


# Generic message
class Message(SQLModel):
    message: str

class ImageMessage(SQLModel):
    message: str
    image_url: str

# JSON payload containing access token
class Token(SQLModel):
    access_token: str
    token_type: str = "bearer"


# Contents of JWT token
class TokenPayload(SQLModel):
    sub: int | None = None


class NewPassword(SQLModel):
    token: str
    new_password: str
