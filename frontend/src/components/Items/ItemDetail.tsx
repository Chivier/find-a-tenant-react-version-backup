import {
    Modal,
    Box,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    Button,
    Textarea,
    FormControl,
    Image,
    FormLabel,
    Input,
  } from "@chakra-ui/react";
  import React from "react";
  import { type ItemOut } from "../../client";
  
  interface ItemDetailProps {
    item: ItemOut;
    isOpen: boolean;
    onClose: () => void;
  }
  
  const ItemDetail: React.FC<ItemDetailProps> = ({ item, isOpen, onClose }) => {
    return (
      <>
        <Modal isOpen={isOpen} onClose={onClose} size={{ base: "sm", md: "md" }} isCentered>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>房源信息</ModalHeader>
            <ModalCloseButton />
            <ModalBody pb={6}>
              <Box width="100%" height="0" paddingBottom="50%" position="relative">
              {item.image_url ? (<Image data-type='Image'
                src={item.image_url}
                alt='Green double couch with wooden legs'
                borderRadius='lg'
                position="absolute"
                width="100%"
                height="100%"
                objectFit="cover"></Image>):
                (<Image data-type='Image'
                src='https://s2.loli.net/2024/03/23/dEG5B73okchAxPl.png'
                alt='Green double couch with wooden legs'
                borderRadius='lg'
                position="absolute"
                width="100%"
                height="100%"
                objectFit="cover"></Image>)
              }
          </Box>
          <FormControl mt={4}>
                <FormLabel>房源类型</FormLabel>
                <Input value={item.property_type || ''} placeholder="学生公寓/社会公寓" type="text" isReadOnly />
              </FormControl>

  
              <FormControl mt={4}>
                <FormLabel>房间类型</FormLabel>
                <Input value={item.room_type || ''} placeholder="studio/ensuite/1b1b" type="text" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>所在城市</FormLabel>
                <Input value={item.city ? item.city : "NA"} type="text" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>公寓名称</FormLabel>
                <Input value={item.title ? item.title : "NA"} type="text" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>邮编</FormLabel>
                <Input value={item.post_code || ''} placeholder="Post Code" type="text" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>价格（GBP pw）</FormLabel>
                <Input value={item.price?.toString() || ''} placeholder="Price" type="number" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>起租日期</FormLabel>
                <Input value={item.begin_date || ''} type="date" isReadOnly />
              </FormControl>
  
              <FormControl mt={4}>
                <FormLabel>结束日期</FormLabel>
                <Input value={item.end_date || ''} type="date" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>是否欢迎拆分租期短租</FormLabel>
                <Input value={item.short_term_allowed === true ? "看情况" : '不欢迎'} placeholder="Duration" type="text" isReadOnly />
              </FormControl>

              
              <FormControl mt={4}>
                <FormLabel>房间描述</FormLabel>
                <Textarea value={item.description || ''} placeholder="Description"  isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>价格描述</FormLabel>
                <Textarea value={item.price_description?.toString() || ''} placeholder="Price" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>时长描述</FormLabel>
                <Textarea value={item.duration_description?.toString() || ''} placeholder="Price" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>额外信息</FormLabel>
                <Textarea value={item.additional_info || ''} placeholder="Duration Description" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>联系邮箱</FormLabel>
                <Input value={item.email || ''} placeholder="Email" type="text" isReadOnly />
              </FormControl>
              
              

              {/*<FormControl mt={4}>*/}
              {/*  <FormLabel>其他联系方式</FormLabel>*/}
              {/*  <Input value={item.contact_info || ''} placeholder="text" type="text" isReadOnly /> */}
              {/*</FormControl>*/}
              

      

              
            </ModalBody>
            <ModalFooter>
              <Button onClick={onClose}>Close</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    )
  }
  
  export default ItemDetail
  