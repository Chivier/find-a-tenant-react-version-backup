import { Button, Flex, useDisclosure } from "@chakra-ui/react"
import type React from "react"
import { type TenantOut } from "../../client"

import TenantDetail from "../Tenants/TenantDetail"

interface DetailButtonProps {
  item: TenantOut;
}

const DetailButton: React.FC<DetailButtonProps> = ({ item }) => {
  const itemDetailModal = useDisclosure()

  return (
    <>
      <Flex py={8} gap={4}>
        {/* TODO: Complete search functionality */}
        {/* <InputGroup w={{ base: '100%', md: 'auto' }}>
                    <InputLeftElement pointerEvents='none'>
                        <Icon as={FaSearch} color='gray.400' />
                    </InputLeftElement>
                    <Input type='text' placeholder='Search' fontSize={{ base: 'sm', md: 'inherit' }} borderRadius='8px' />
                </InputGroup> */}
        
        <Button
          variant="primary"
          gap={1}
          fontSize={{ base: "sm", md: "inherit" }}
          onClick={itemDetailModal.onOpen}
        >
          View Details
        </Button>
        <TenantDetail item={item} isOpen={itemDetailModal.isOpen} onClose={itemDetailModal.onClose} />
      </Flex>
    </>
  )
}

export default DetailButton
