import {
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    Button,
    FormControl,
    FormLabel,
    Textarea,
    Input,
  } from "@chakra-ui/react";
  import React from "react";
  import { type TenantOut } from "../../client";
  
  interface TenantDetailProps {
    item: TenantOut;
    isOpen: boolean;
    onClose: () => void;
  }
  
  const TenantDetail: React.FC<TenantDetailProps> = ({ item, isOpen, onClose }) => {
    return (
      <>
        <Modal isOpen={isOpen} onClose={onClose} size={{ base: "sm", md: "md" }} isCentered>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>求租信息</ModalHeader>
            <ModalCloseButton />
            <ModalBody pb={6}>
              <FormControl mt={4}>
                <FormLabel>邮箱</FormLabel>
                <Input value={item.email || ''} placeholder="Email" type="text" isReadOnly />
              </FormControl>


              <FormControl mt={4}>
                <FormLabel>城市</FormLabel>
                <Input value={item.city} type="text" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>区域/位置</FormLabel>
                <Input value={item.post_code ? item.post_code:'not given'} type="text" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>开始日期</FormLabel>
                <Input value={item.begin_date || ''} type="date" isReadOnly />
              </FormControl>
  
              <FormControl mt={4}>
                <FormLabel>结束日期</FormLabel>
                <Input value={item.end_date || ''} type="date" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>预算</FormLabel>
                <Input value={item.price_upper_bound?.toString() || ''} placeholder="Price" type="number" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>房源类型</FormLabel>
                <Input value={item.property_type || ''} placeholder="Property Type" type="text" isReadOnly />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>求租类型</FormLabel>
                <Input value={item.let_type || ''} placeholder="Property Type" type="text" isReadOnly />
              </FormControl>
  
              {/*<FormControl mt={4}>*/}
              {/*  <FormLabel>联系方式</FormLabel>*/}
              {/*  <Input value={item.contact_info || ''} placeholder="未给出" type="text" isReadOnly />*/}
              {/*</FormControl>*/}

              <FormControl mt={4}>
                <FormLabel>其他要求/信息</FormLabel>
                <Textarea value={item.description || ''} placeholder="Description" isReadOnly />
              </FormControl>
  
             
  
  
              

              
  
  
             
  
              
            </ModalBody>
            <ModalFooter>
              <Button onClick={onClose}>Close</Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    )
  }
  
  export default TenantDetail
  