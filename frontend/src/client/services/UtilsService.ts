/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Body_utils_upload_image } from '../models/Body_utils_upload_image';
import type { ImageMessage } from '../models/ImageMessage';
import type { Message } from '../models/Message';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class UtilsService {

    /**
     * Test Email
     * Test emails.
     * @returns Message Successful Response
     * @throws ApiError
     */
    public static testEmail({
        emailTo,
    }: {
        emailTo: string,
    }): CancelablePromise<Message> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/v1/utils/test-email/',
            query: {
                'email_to': emailTo,
            },
            errors: {
                422: `Validation Error`,
            },
        });
    }

    /**
     * Upload Image
     * Upload an image.
     * @returns ImageMessage Successful Response
     * @throws ApiError
     */
    public static uploadImage({
        formData,
    }: {
        formData: Body_utils_upload_image,
    }): CancelablePromise<ImageMessage> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/v1/utils/upload-image/',
            formData: formData,
            mediaType: 'multipart/form-data',
            errors: {
                422: `Validation Error`,
            },
        });
    }

}
