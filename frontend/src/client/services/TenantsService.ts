/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Message } from '../models/Message';
import type { TenantCreate } from '../models/TenantCreate';
import type { TenantOut } from '../models/TenantOut';
import type { TenantsOut } from '../models/TenantsOut';
import type { TenantUpdate } from '../models/TenantUpdate';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class TenantsService {

    /**
     * Read Items
     * Retrieve items.
     * @returns TenantsOut Successful Response
     * @throws ApiError
     */
    public static readItems({
        skip,
        limit = 100,
        showAll = false,
    }: {
        skip?: number,
        limit?: number,
        showAll?: boolean,
    }): CancelablePromise<TenantsOut> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/v1/tenants/tenants',
            query: {
                'skip': skip,
                'limit': limit,
                'show_all': showAll,
            },
            errors: {
                422: `Validation Error`,
            },
        });
    }

    /**
     * Read Item
     * Get item by ID.
     * @returns TenantOut Successful Response
     * @throws ApiError
     */
    public static readItem({
        id,
    }: {
        id: number,
    }): CancelablePromise<TenantOut> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/api/v1/tenants/tenant/{id}',
            path: {
                'id': id,
            },
            errors: {
                422: `Validation Error`,
            },
        });
    }

    /**
     * Update Item
     * Update an item.
     * @returns TenantOut Successful Response
     * @throws ApiError
     */
    public static updateItem({
        id,
        requestBody,
    }: {
        id: number,
        requestBody: TenantUpdate,
    }): CancelablePromise<TenantOut> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/api/v1/tenants/tenant/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                422: `Validation Error`,
            },
        });
    }

    /**
     * Delete Item
     * Delete an item.
     * @returns Message Successful Response
     * @throws ApiError
     */
    public static deleteItem({
        id,
    }: {
        id: number,
    }): CancelablePromise<Message> {
        return __request(OpenAPI, {
            method: 'DELETE',
            url: '/api/v1/tenants/tenant/{id}',
            path: {
                'id': id,
            },
            errors: {
                422: `Validation Error`,
            },
        });
    }

    /**
     * Create Item
     * Create new item.
     * @returns TenantOut Successful Response
     * @throws ApiError
     */
    public static createItem({
        requestBody,
    }: {
        requestBody: TenantCreate,
    }): CancelablePromise<TenantOut> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/v1/tenants/tenant',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                422: `Validation Error`,
            },
        });
    }

}
