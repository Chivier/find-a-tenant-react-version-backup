import {
  Container,
  Flex,
  Heading,
  Spinner,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react"
import { createFileRoute } from "@tanstack/react-router"
import { useQuery } from "react-query"

import { type ApiError, TenantsService } from "../../client"
import ActionsMenu from "../../components/Common/ActionsMenu"
import Navbar from "../../components/Common/Navbar"
import useCustomToast from "../../hooks/useCustomToast"

export const Route = createFileRoute("/_layout/tenant_post")({
  component: Items,
})

function Items() {
  const showToast = useCustomToast()
  const {
    data: tenants,
    isLoading,
    isError,
    error,
  } = useQuery("tenants_post", () => TenantsService.readItems({showAll: false}))

  if (isError) {
    const errDetail = (error as ApiError).body?.detail
    showToast("Something went wrong.", `${errDetail}`, "error")
  }

  return (
    <>
      {isLoading ? (
        // TODO: Add skeleton
        <Flex justify="center" align="center" height="100vh" width="full">
          <Spinner size="xl" color="ui.main" />
        </Flex>
      ) : (
        tenants && (
          <Container maxW="full">
            <Heading
              size="lg"
              textAlign={{ base: "center", md: "left" }}
              pt={12}
            >
              求租管理
            </Heading>
            <Navbar type={"Your info"} />
            <TableContainer>
              <Table size={{ base: "sm", md: "md" }}>
                <Thead>
                  <Tr>
                    <Th>ID</Th>
                    <Th>城市</Th>
                    
                    <Th>开始日期</Th>
                    <Th>结束日期</Th>
                    
                  </Tr>
                </Thead>
                <Tbody>
                  {tenants.data.map((tenant) => (
                    <Tr key={tenant.id}>
                      <Td>{tenant.id}</Td>
                      <Td>{tenant.city}</Td>
                      
                      <Td color={!tenant.begin_date ? "gray.400" : "inherit"}>
                        {tenant.begin_date || "N/A"}
                      </Td>
                      
                      <Td color={!tenant.end_date ? "gray.400" : "inherit"}>
                        {tenant.end_date || "N/A"}
                      </Td>
                      
                      <Td>
                        <ActionsMenu type={"Item"} value={tenant} />
                      </Td>
                    </Tr>
                  ))}
                </Tbody>
              </Table>
            </TableContainer>
          </Container>
        )
      )}
    </>
  )
}

export default Items
