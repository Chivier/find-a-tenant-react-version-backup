import {
  Container,
  Flex,
  Spinner,
  Heading,
  Box,
  Text,
  Select,
  CardFooter,
  Stack,
  Image,
  CardBody,
  Grid,
  
  Card
} from "@chakra-ui/react"
import { createFileRoute } from "@tanstack/react-router"
import { useQuery } from "react-query"
// import  {type ApiError, ItemsService, UserOut, TenantsService } from "../../client"
import  {type ApiError, TenantsService } from "../../client"
import useCustomToast from "../../hooks/useCustomToast"
import DetailButton from "../../components/Common/TenantDetailButton"
import { useState } from 'react';


export const Route = createFileRoute("/_layout/tenant")({
  component: Dashboard,
})

function Dashboard() {
  const [selectedCity, setSelectedCity] = useState('');

  // const currentUser = queryClient.getQueryData<UserOut>("currentUser")
  const showToast = useCustomToast()
  const {
    data: tenants,
    isLoading,             
    isError,
    error,
  } = useQuery("tenants", () => TenantsService.readItems({showAll: true}))

  const filteredItems = selectedCity ? tenants?.data.filter(product => product.city === selectedCity) : tenants?.data;

  if (isError) {
    const errDetail = (error as ApiError).body?.detail
    showToast("Something went wrong.", `${errDetail}`, "error")
  }

  return (
    <>
    {isLoading ? (
        // TODO: Add skeleton
        <Flex justify="center" align="center" height="100vh" width="full">
          <Spinner size="xl" color="ui.main" />
        </Flex>
      ) : (
        tenants && (
    <Container maxW="full">
      <Box pt={12} m={2}>
          <Flex justifyContent="l" gap="4">
          <Image data-type='Image'
              src='https://s2.loli.net/2024/04/11/OAZvN2QEUW5Lkxp.jpg'
              alt='Green double couch with wooden legs'
              boxSize="150px" objectFit="cover"></Image>
        
          <Image data-type='Image' src="https://s2.loli.net/2024/04/11/oj9vEg1MtSrXuix.png" alt="描述2" boxSize="150px" objectFit="cover" />

          </Flex>
          <Text>用爱发电！自费开发！大家的打赏支持真的很重要😭</Text>
          <br/>
          <Select placeholder="全部" onChange={(e) => setSelectedCity(e.target.value)}>
            {tenants.cities.map((city, index) => (
              <option key={index} value={city}>{city}</option>
            ))}
        </Select> 
        </Box>

        

      <Grid templateColumns={{ sm: "repeat(1, 1fr)", md: "repeat(2, 1fr)", lg: "repeat(3, 1fr)", xl: "repeat(4, 1fr)" }} gap={6} m={2}>
        {filteredItems?.map((tenant) => (
          <Card key={tenant.id} data-type='Card' maxW='sm'>
          <CardBody data-type='CardBody'>
            <Stack data-type='Stack' mt='6' spacing='3'>

              <Heading data-type='Heading' size='md'>{tenant.city}</Heading>
              
              <Text data-type='Text'>
                {tenant.description}
              </Text>

              <Text data-type='Text' fontSize='xl'>
                {tenant.begin_date} 至 {tenant.end_date}
              </Text>

              <Text data-type='Text' color='blue.600' fontSize='2xl'>
                预算：{tenant.price_upper_bound? tenant.price_upper_bound >0 ? tenant.price_upper_bound : '未给出': '未给出'}
              </Text>
            </Stack>
          </CardBody>
          <CardFooter data-type='CardFooter'>
            
            <DetailButton item={tenant} />
          </CardFooter>
        </Card>
        ))}
       
      </Grid>
      
    </Container>
    )
    )}
    </>
  )
  }

export default Dashboard

