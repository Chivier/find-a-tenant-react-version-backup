import {
    Container,
    Flex,
    Spinner,
    Heading,
    Box,
    Text,
    Image,
    Input,
    CardFooter,
    Stack,
    Select,
    CardBody,
    Grid,
    Card, Center, GridItem, Button, Highlight
} from "@chakra-ui/react"
import {createFileRoute} from "@tanstack/react-router"
// import { useQueryClient } from "react-query"
import {useQuery} from "react-query"
import {type ApiError, ItemOut, ItemsService} from "../../client"
import useCustomToast from "../../hooks/useCustomToast"
import DetailButton from "../../components/Common/DetailButton"
import {useState} from 'react';

export const Route = createFileRoute("/_layout/")({
    component: Dashboard,
})

const compareDates = (date1: string, date2: string): number => {
    const dateA = new Date(date1);
    const dateB = new Date(date2);
    return dateA.getTime() - dateB.getTime();
};
const isNumber = (value: string): boolean => {
    return !isNaN(Number(value));
};

function Dashboard() {
    const [selectedCity, setSelectedCity] = useState('');
    const [selectedBeginDate, setSelectedBeginDate] = useState('');
    const [selectedEndDate, setSelectedEndDate] = useState('');
    const [selectedPriceLowerBound, setSelectedPriceLowerBound] = useState('');
    const [selectedPriceUpperBound, setSelectedPriceUpperBound] = useState('');
    const [errorinfo, setErrorinfo] = useState('');

    // const currentUser = queryClient.getQueryData<UserOut>("currentUser")
    const showToast = useCustomToast()
    const {
        data: items_show,
        isLoading,
        isError,
        error,
    } = useQuery("items", () => ItemsService.readItems({showAll: true}));


    // let filteredItems = items_show?.data;
    // @ts-ignore
    // let initial_filter = [...items_show.data];
    // initial_filter = initial_filter.reverse();
    const [filteredItems, setFilteredItems] = useState([] as ItemOut[]);

    const updateFilter = () => {
        if (selectedBeginDate && selectedEndDate) {
            if (compareDates(selectedBeginDate, selectedEndDate) > 0) {
                setErrorinfo('开始日期不能晚于结束日期！');
                showToast("Something went wrong.", errorinfo, "error");
            }
        }
        if (selectedPriceLowerBound) {
            if (!isNumber(selectedPriceLowerBound)) {
                setErrorinfo('价格下限必须为数字！');
                showToast("Something went wrong.", errorinfo, "error");
            }
        }
        if (selectedPriceUpperBound) {
            if (!isNumber(selectedPriceUpperBound)) {
                setErrorinfo('价格上限必须为数字！');
                showToast("Something went wrong.", errorinfo, "error");
            }
        }
        if (selectedPriceLowerBound && parseFloat(selectedPriceLowerBound) < 0) {
            setErrorinfo('价格不能为负数！');
            showToast("Something went wrong.", errorinfo, "error")
        }
        if (selectedPriceUpperBound && parseFloat(selectedPriceUpperBound) < 0) {
            setErrorinfo('价格不能为负数！');
            showToast("Something went wrong.", errorinfo, "error")
        }
        if (selectedPriceLowerBound && selectedPriceUpperBound) {
            if (parseFloat(selectedPriceLowerBound) > parseFloat(selectedPriceUpperBound)) {
                setErrorinfo('价格下限不能高于价格上限！');
                showToast("Something went wrong.", errorinfo, "error")
            }
        }

        // @ts-ignore
        let next_filter = [...items_show.data];
        next_filter = next_filter.reverse();

        if (selectedCity) {
            // @ts-ignore
            next_filter = next_filter.filter(product => product.city === selectedCity);
        }

        if (selectedBeginDate) {
            // @ts-ignore
            next_filter = next_filter.filter(product => (compareDates(product.begin_date, selectedBeginDate) <= 0))
        }
        if (selectedEndDate) {
            // @ts-ignore
            next_filter = next_filter.filter(product => (compareDates(product.end_date, selectedEndDate) >= 0))
        }
        if (selectedPriceLowerBound) {
            const lowerBound = parseFloat(selectedPriceLowerBound);
            // @ts-ignore
            next_filter = next_filter.filter(product => (product.price >= lowerBound))
        }
        if (selectedPriceUpperBound) {
            const upperBound = parseFloat(selectedPriceUpperBound);
            // @ts-ignore
            next_filter = next_filter.filter(product => (product.price <= upperBound))
        }
        setFilteredItems(next_filter);
    }
    // const filteredItems = selectedCity ? items_show?.data.filter(product => product.city === selectedCity) : items_show?.data;

    if (isError) {
        const errDetail = (error as ApiError).body?.detail
        showToast("Something went wrong.", `${errDetail}`, "error")
    }

    return (
        <>
            {isLoading ? (
                // TODO: Add skeleton
                <Flex justify="center" align="center" height="100vh" width="full">
                    <Spinner size="xl" color="ui.main"/>
                </Flex>
            ) : (
                items_show && (
                    <Container maxW="full">
                        <Box pt={12} m={2}>
                            <Center>
                                <Flex justifyContent="l" gap="4">
                                    <Image data-type='Image'
                                           src='https://s2.loli.net/2024/04/11/OAZvN2QEUW5Lkxp.jpg'
                                           alt='Green double couch with wooden legs'
                                           boxSize="150px" objectFit="cover"></Image>
                                    <Image data-type='Image' src="https://s2.loli.net/2024/04/11/oj9vEg1MtSrXuix.png"
                                           alt="描述2" boxSize="150px" objectFit="cover"/>
                                </Flex>
                            </Center>
                            <br/>
                            <Center>
                                <Heading as='h4' size='md'>
                                    <Highlight
                                        query={['广告位招租']}
                                        styles={{px: '2', py: '1', rounded: 'full', bg: 'red.100'}}
                                    >
                                        广告位招租
                                    </Highlight>
                                </Heading>
                            </Center>
                            <br/>
                            <Center>
                                <Text>用爱发电！自费开发！大家的打赏支持真的很重要😭</Text>
                            </Center>
                            <br/>
                            <Heading>
                                房源检索

                            </Heading>

                            <Box>
                                <Heading as='h4' size='md' marginY={2}>
                                    城市筛选
                                </Heading>
                                <Select placeholder="全部" onChange={(e) => setSelectedCity(e.target.value)}
                                        value={selectedCity}>
                                    {items_show.cities.map((city, index) => (
                                        <option key={index} value={city}>{city}</option>
                                    ))}
                                </Select>
                            </Box>
                            <Heading as='h4' size='md' marginY={2}>
                                日期筛选
                            </Heading>
                            <Grid
                                gap={2}
                                templateColumns='repeat(2, 1fr)'>
                                <GridItem>
                                    <Input placeholder='Select Begin Date' size='md' type='date'
                                           value={selectedBeginDate}
                                           onChange={(e) => setSelectedBeginDate(e.target.value)}/>
                                </GridItem>
                                <GridItem>
                                    <Input placeholder='Select End Date' size='md' type='date'
                                           value={selectedEndDate}
                                           onChange={(e) => setSelectedEndDate(e.target.value)}/>
                                </GridItem>
                            </Grid>
                            <Heading as='h4' size='md' marginY={2}>
                                价格筛选（per week）
                            </Heading>
                            <Grid
                                templateColumns='repeat(2, 1fr)'
                            gap={2}>
                                <Input placeholder={'Price Lower Bound'}
                                       value={selectedPriceLowerBound}
                                       onChange={(e) => {
                                           setSelectedPriceLowerBound(e.target.value)
                                       }}
                                />
                                <Input placeholder={'Price Upper Bound'}
                                       value={selectedPriceUpperBound}
                                       onChange={(e) => {
                                           setSelectedPriceUpperBound(e.target.value)
                                       }}
                                />
                            </Grid>
                            <Grid
                                templateColumns='repeat(2, 1fr)'
                                gap={2}>
                                <Button marginY={4}
                                        variant="primary"
                                        onClick={
                                    () => {
                                        updateFilter()
                                    }
                                }>
                                    检索
                                </Button>
                                <Button marginY={4}
                                        variant="primary"
                                        onClick={
                                    () => {
                                        setSelectedCity('');
                                        setSelectedBeginDate('');
                                        setSelectedEndDate('');
                                        setSelectedPriceLowerBound('');
                                        setSelectedPriceUpperBound('');
                                        setErrorinfo('');
                                        let next_initial = items_show.data;
                                        next_initial = next_initial.reverse();
                                        setFilteredItems(next_initial);
                                    }
                                }>
                                    清空检索条件
                                </Button>
                            </Grid>
                        </Box>
                        {
                            errorinfo && (
                                <Highlight
                                    query={['error']}
                                    styles={{px: '2', py: '1', rounded: 'full', bg: 'red.100'}}
                                >
                                    {errorinfo}
                                </Highlight>
                            )
                        }
                        <br/>


                        {
                            filteredItems?.length === 0 ? (
                                <Center>
                                <Heading as='h4' size='md' marginY={2}>
                                    请输入或更换筛选条件
                                </Heading>
                                </Center>
                            ) : (
                                <Grid templateColumns={{
                                    sm: "repeat(1, 1fr)",
                                    md: "repeat(2, 1fr)",
                                    lg: "repeat(3, 1fr)",
                                    xl: "repeat(4, 1fr)"
                                }} gap={6} m={2}>
                                    {
                                        // @ts-ignore
                                        filteredItems?.map((product) => (
                                        <Card data-type='Card' maxW='sm'>
                                            <CardBody data-type='CardBody'>
                                                <Box width="100%" height="0" paddingBottom="50%" position="relative">
                                                    {product.image_url ? (<Image data-type='Image'
                                                                                 src={product.image_url}
                                                                                 alt='Green double couch with wooden legs'
                                                                                 borderRadius='lg'
                                                                                 position="absolute"
                                                                                 width="100%"
                                                                                 height="100%"
                                                                                 objectFit="cover"></Image>) :
                                                        (<Image data-type='Image'
                                                                src='https://s2.loli.net/2024/03/23/dEG5B73okchAxPl.png'
                                                                alt='Green double couch with wooden legs'
                                                                borderRadius='lg'
                                                                position="absolute"
                                                                width="100%"
                                                                height="100%"
                                                                objectFit="cover"></Image>)
                                                    }
                                                </Box>
                                                <Stack data-type='Stack' mt='3' spacing='3'>
                                                    <Heading data-type='Heading' size='md'>{product.title}</Heading>

                                                    <Text> {product.city} </Text>

                                                    <Text data-type='Text'>
                                                        {product.description}
                                                    </Text>

                                                    <Text>
                                                        {product.begin_date} - {product.end_date}
                                                    </Text>

                                                    <Text data-type='Text' color='blue.600' fontSize='2xl'>
                                                        {product.price} pw
                                                    </Text>

                                                </Stack>
                                            </CardBody>
                                            <CardFooter data-type='CardFooter'>

                                                <DetailButton item={product}/>
                                            </CardFooter>
                                        </Card>
                                    ))}

                                </Grid>)
                        }

                    </Container>
                )
            )}
        </>
    )
}

export default Dashboard

