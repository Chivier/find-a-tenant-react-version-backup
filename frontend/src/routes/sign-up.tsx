import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
} from "@chakra-ui/react"

import {
  Container,
  Image,
  InputGroup,
  InputRightElement,
  useBoolean,
  Button,
} from "@chakra-ui/react"


import { useState, useEffect } from "react";
import { useForm } from "react-hook-form"

import {
  createFileRoute,
} from "@tanstack/react-router"



import Logo from "../assets/images/fastapi-logo.svg"
import {SignupService} from "../client"

import { type UserCreate, UsersService } from "../client"

export const Route = createFileRoute("/sign-up")({
  component: SignUp
})

function generateRandomNumber(): string {
  // Generate a random 6-digit number
  const randomNumber = Math.floor(100000 + Math.random() * 900000);
  return randomNumber.toString();
}

function setCookie(name: string, value: string, minutes: number) {
  const expirationDate = new Date();
  expirationDate.setTime(expirationDate.getTime() + minutes * 60 * 1000);
  const expires = `expires=${expirationDate.toUTCString()}`;
  document.cookie = `${name}=${value};${expires};path=/`;
}

function getCookie(name: string): string | null {
  const cookies = document.cookie.split(';');
  for (let cookie of cookies) {
    const [cookieName, cookieValue] = cookie.split('=');
    if (cookieName.trim() === name) {
      return cookieValue;
    }
  }
  return null;
}


function SignUp() {
  interface UserCreateForm extends UserCreate {
    confirm_password: string
    verification_code: string
  }



  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors, isSubmitting },
  } = useForm<UserCreateForm>({
    mode: "onBlur",
    criteriaMode: "all",
    defaultValues: {
      email: "",
      full_name: "",
      password: "",
      confirm_password: "",
      verification_code: "",
      is_superuser: false,
      is_active: true,
    },
  })

  const [show, setShow] = useBoolean(false);
  const [verificationSent, setVerificationSent] = useState(false);
  const [cooldown, setCooldown] = useState(0);
  // const [registerSuccess, setRegisterSuccess] = useState(false);

  useEffect(() => {
    let interval: NodeJS.Timeout;
    if (cooldown > 0) {
      interval = setInterval(() => {
        setCooldown((prevCooldown) => prevCooldown - 1);
      }, 1000);
    }
    return () => clearInterval(interval);
  }, [cooldown]);

  const onSubmit = async (data: UserCreateForm) => {
    if (getCookie("verification_code") !== data.verification_code) {
      alert("Invalid verification code");
      return;
    }

    const response = await UsersService.createUserOpen({ requestBody: data });
    if (response.id) {
      // setRegisterSuccess(true);
      // Send email to the user
      window.location.href='/login';
    } else {
      alert("Failed to create user");
    }
  }

  const onVerify = async (data: UserCreateForm) => {
    const randomNumber = generateRandomNumber();
    setCookie("verification_code", randomNumber, 5);
    // Send the verification code to the user
    await SignupService.sendVerificationCode({
      email: data.email,
      code: randomNumber,
    });
    setVerificationSent(true);
    setCooldown(60);
  }

  return (
    <>
      <Container
        as="form"
        onSubmit={handleSubmit(onSubmit)}
        h="100vh"
        maxW="sm"
        alignItems="stretch"
        justifyContent="center"
        gap={4}
        centerContent
      >
        <Image
          src={Logo}
          alt="FastAPI logo"
          height="auto"
          maxW="2xs"
          alignSelf="center"
          mb={4}
        />
        <FormControl id="email" isInvalid={!!errors.email}>
          <FormLabel>Email</FormLabel>
          <Input
            {...register("email", {
              required: "Email is required",
              pattern: {
                value: /^\S+@\S+$/i,
                message: "Invalid email address",
              },
            })}
            type="email"
          />
          {errors.email && (
            <FormErrorMessage>{errors.email.message}</FormErrorMessage>
          )}
        </FormControl>
        <FormControl id="full_name" isInvalid={!!errors.full_name}>
          <FormLabel>Full Name</FormLabel>
          <Input
            {...register("full_name", {
              required: "Full Name is required",
            })}
            type="text"
          />
          {errors.full_name && (
            <FormErrorMessage>{errors.full_name.message}</FormErrorMessage>
          )}
        </FormControl>
        <FormControl id="password" isInvalid={!!errors.password}>
          <FormLabel>Password</FormLabel>
          <InputGroup>
            <Input
              {...register("password", {
                required: "Password is required",
                minLength: {
                  value: 8,
                  message: "Password must be at least 8 characters",
                },
              })}
              type={show ? "text" : "password"}
            />
            <InputRightElement width="4.5rem">
              <Button h="1.75rem" size="sm" onClick={setShow.toggle}>
                {show ? "Hide" : "Show"}
              </Button>
            </InputRightElement>
          </InputGroup>
          {errors.password && (
            <FormErrorMessage>{errors.password.message}</FormErrorMessage>
          )}
        </FormControl>
        <FormControl id="confirm_password" isInvalid={!!errors.confirm_password}>
          <FormLabel>Confirm Password</FormLabel>
          <Input
            {...register("confirm_password", {
              required: "Confirm Password is required",
              validate: (value) =>
                value === getValues("password") || "Passwords do not match",
            })}
            type={show ? "text" : "password"}
          />
          {errors.confirm_password && (
            <FormErrorMessage>{errors.confirm_password.message}</FormErrorMessage>
          )}
        </FormControl>
        {!verificationSent ? (
          <FormControl id="verification_code" isInvalid={!!errors.verification_code}>
            <FormLabel>Verification Code</FormLabel>
            <InputGroup>
              <Input
                {...register("verification_code", {
                })}
                type="text"
              />
              <InputRightElement width="4.5rem">
                <Button
                  colorScheme="blue"
                  isLoading={cooldown > 0}
                  onClick={handleSubmit(onVerify)}
                  isDisabled={cooldown > 0}
                  size="sm"
                  rightIcon={cooldown > 0 ? <span>{cooldown}</span> : undefined}
                >
                  {cooldown > 0 ? "Sending..." : "Send"}
                </Button>
              </InputRightElement>
            </InputGroup>
            {errors.verification_code && (
              <FormErrorMessage>{errors.verification_code.message}</FormErrorMessage>
            )}
          </FormControl>
        ) : (
          <FormControl id="verification_code" isInvalid={!!errors.verification_code}>
            <FormLabel>Verification Code</FormLabel>
            <Input
              {...register("verification_code", {
              })}
              type="text"
            />
            {errors.verification_code && (
              <FormErrorMessage>{errors.verification_code.message}</FormErrorMessage>
            )}
          </FormControl>
        )}
        <Button
          type="submit"
          variant="primary"
          isLoading={isSubmitting}
          mt={4}
        >
          Sign Up
        </Button>
      </Container>
    </>
  )
}

export default SignUp
